#! /bin/bash

kubectl apply -f kubernetes/mysql-storage.yaml

kubectl apply -f kubernetes/service.yaml

kubectl apply -f kubernetes/db-secret.yaml

kubectl apply -f kubernetes/initdb-configmap.yaml

kubectl apply -f kubernetes/deployment.yaml