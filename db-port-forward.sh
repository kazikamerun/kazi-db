#! /bin/bash

KAZI_POD=$(kubectl get pod -l app=mysql -o jsonpath="{.items[0].metadata.name}")

echo "Forward Kazi Pod zu $KAZI_POD \n"

kubectl port-forward $KAZI_POD 3306:3306

