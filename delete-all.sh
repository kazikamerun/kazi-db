#! /bin/bash

kubectl delete -n default deployment kazi-db

kubectl delete service kazi-db

kubectl delete secret kazi-db-secret

kubectl delete configmap kazi-initdb

kubectl delete PersistentVolumeClaim mysql-pv-claim

kubectl delete PersistentVolume mysql-pv-volume